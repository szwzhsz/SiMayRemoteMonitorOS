﻿using SiMay.ReflectCache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Core
{
    public class DesktopViewDescribePacket : EntitySerializerBase
    {
        public string MachineName { get; set; }
        public string RemarkInformation { get; set; }
    }
}
